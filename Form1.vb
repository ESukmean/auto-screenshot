﻿Public Class Form1
    Dim times As Int16 = 0
    Private WithEvents timers As New Timer
    Dim last As String
    Private Function TakeShotOfScreens() As Bitmap
        '출처: http://www.dreamincode.net/code/snippet2122.htm
        Dim maxHeight As Integer = 0
        Dim maxWidth As Integer = 0
        For Each scr As Screen In Screen.AllScreens
            maxWidth += scr.Bounds.Width
            If scr.Bounds.Height > maxHeight Then maxHeight = scr.Bounds.Height
        Next
        Dim allScreensCapture As New Bitmap(maxWidth, maxHeight, System.Drawing.Imaging.PixelFormat.Format32bppRgb)
        Dim screenGrab As Bitmap
        Dim screenSize As Size
        Dim g As Graphics
        Dim g2 As Graphics = Graphics.FromImage(allScreensCapture)
        Dim a As New Point(0, 0)


        For Each scr As Screen In Screen.AllScreens
            screenSize = New Size(scr.Bounds.Width, scr.Bounds.Height)
            screenGrab = New Bitmap(scr.Bounds.Width, scr.Bounds.Height)
            g = Graphics.FromImage(screenGrab)
            g.CopyFromScreen(a, New Point(0, 0), screenSize)
            g2.DrawImage(screenGrab, a)
            a.X += scr.Bounds.Width
        Next
        If CheckBox2.Checked = True Then
            Dim nTicks As Double
            Dim nDays As Integer
            Dim nHours As Integer
            Dim nMin As Integer
            Dim nSec As Integer
            nTicks = Environment.TickCount
            nTicks = nTicks / 1000
            nDays = Int(nTicks / (3600 * 24))
            nTicks = nTicks - (Int(nTicks / (3600 * 24)) * (3600 * 24))
            nHours = Int(nTicks / 3600)
            nTicks = nTicks - (Int(nTicks / 3600) * 3600)
            nMin = Int(nTicks / 60)
            nTicks = nTicks - (Int(nTicks / 60) * 60)
            nSec = nTicks
            Dim myGR As Graphics = Graphics.FromImage(allScreensCapture)
            ' Create string to draw.
            Dim drawString As [String] = "부팅후 " + nDays.ToString + "일 " + nHours.ToString + "시간 " + nMin.ToString + "분 " + nSec.ToString + "초 "
            ' Create font and brush.
            Dim drawFont As New Font("굴림", 16, FontStyle.Bold)
            Dim drawBrush As New SolidBrush(Color.White)
            ' Create point for upper-left corner of drawing.
            Dim drawPoint As New PointF(290.0F, 10.0F)
            ' Draw string to screen.

            myGR.DrawString(drawString, drawFont, drawBrush, drawPoint)

            drawFont = New Font("굴림", 16, FontStyle.Bold)
            drawPoint = New PointF(10.0F, 10.0F)
            drawBrush = New SolidBrush(Color.Black)
            myGR.DrawString(drawString, drawFont, drawBrush, drawPoint)

            drawFont = New Font("굴림", 16, FontStyle.Bold)
            drawPoint = New PointF(570.0F, 10.0F)
            drawBrush = New SolidBrush(Color.Brown)
            myGR.DrawString(drawString, drawFont, drawBrush, drawPoint)

            drawFont = New Font("굴림", 16, FontStyle.Bold)
            drawPoint = New PointF(850.0F, 10.0F)
            drawBrush = New SolidBrush(Color.Red)
            myGR.DrawString(drawString, drawFont, drawBrush, drawPoint)

            drawFont = New Font("굴림", 16, FontStyle.Bold)
            drawPoint = New PointF(1130.0F, 10.0F)
            drawBrush = New SolidBrush(Color.Yellow)
            myGR.DrawString(drawString, drawFont, drawBrush, drawPoint)

            myGR.Dispose()
            drawBrush.Dispose()
            drawFont.Dispose()
            drawPoint = Nothing
            drawString = Nothing
            myGR = Nothing
            drawBrush = Nothing
            drawFont = Nothing
        End If
        

        ' Draw string to screen.


        Application.DoEvents()
        Return allScreensCapture
    End Function
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Sel_Di.Click
        Dim direct_di As New FolderBrowserDialog
        direct_di.Description = "스크린샷을 저장할 폴더"
A:      direct_di.ShowDialog()

        If My.Computer.FileSystem.DirectoryExists(direct_di.SelectedPath) = False Then
            If MsgBox("폴더의 경로가 잘못되었거나, 폴더가 선택되지 않았습니다. 다시 선택하시겠습니까?", MsgBoxStyle.YesNo, "경로 또는 폴더없음") = MsgBoxResult.Yes Then
                GoTo a
            End If
            If direct_di.SelectedPath = Nothing Or "" Then
                direct_di.Dispose()
                direct_di = Nothing
                Exit Sub
            End If
        End If

        Txb_folder.Text = direct_di.SelectedPath

        direct_di = Nothing

    End Sub

    Private Sub NumericUpDown1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDown1.ValueChanged
        If TrackBar1.Value > NumericUpDown1.Value Then
            TrackBar1.Maximum = NumericUpDown1.Value
            TrackBar1.Value = NumericUpDown1.Value - 1
        End If
        TrackBar1.Maximum = NumericUpDown1.Value

    End Sub
    Private Sub timers_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles timers.Tick
        Try
            TrackBar1.Value += 1
        Catch ex As Exception
            If TrackBar1.Value >= TrackBar1.Maximum Then
                TrackBar1.Value = TrackBar1.Maximum
            End If
            TrackBar1_ValueChanged(Nothing, Nothing)
        End Try


    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_start_cap.Click
        Dim times_For_stck As Integer = 0
        Try
            If Command.ToString = "-hide" Then
                times_For_stck += 1

                Me.Hide()
                Me.Visible = False

            End If
        Catch ex As Exception
            MsgBox(times_For_stck.ToString)
        End Try

        If My.Computer.FileSystem.DirectoryExists(Txb_folder.Text) = False Then
            If MsgBox("선택하신 경로에 폴더가 없습니다. 만드시겠습니까?", MsgBoxStyle.YesNo, "폴더 없음") = MsgBoxResult.Yes Then

                Dim path() As String = Split(Txb_folder.Text, "\")
                Dim madePath As String = ""
                Dim times As Integer = 0
                Do Until My.Computer.FileSystem.DirectoryExists(Txb_folder.Text) = True

                    madePath = madePath + path(times) + "\"
                    Try

                        If My.Computer.FileSystem.DirectoryExists(madePath) = False Then
                            My.Computer.FileSystem.CreateDirectory(madePath)
                        End If

                    Catch ex As Exception
                        MsgBox("폴더 '" + madePath + "'를 만드는 과정에 오류가 났습니다." + vbCrLf + "오류내용:" + vbCrLf + ex.Message, MsgBoxStyle.Critical, "폴더 제작 오류")
                        Exit Sub
                    End Try
                    times += 1
                Loop
            Else
                Exit Sub

            End If

            My.Settings.last_path = Txb_folder.Text
            My.Settings.num_max = NumericUpDown1.Value
        End If
        Select Case Mid(ComboBox1.Text, 1, 4)
            Case ".png"
                Exit Select
            Case ".jpg"
                Exit Select
            Case ".bmp"
                Exit Select
            Case ".gif"
                Exit Select
            Case Else
                MessageBox.Show("확장자명이 올바르지 않습니다. 올바른 확장자를 넣어 주세요.", "확장자 경고", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboBox1.Focus()
                Exit Sub
        End Select
        Btn_start_cap.Enabled = False
        btn_stop_cap.Enabled = True
        TrackBar1.Enabled = True
        timers.Interval = 1000
        timers.Start()
    End Sub

    Private Sub Button4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_stop_cap.Click
        TrackBar1.Value = 0
        TrackBar1.Enabled = False
        timers.Stop()
        Btn_start_cap.Enabled = True
        btn_stop_cap.Enabled = False
    End Sub

    Private Sub TrackBar1_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TrackBar1.ValueChanged
        If Not TrackBar1.Value = TrackBar1.Maximum Then
            Exit Sub
        End If


        TrackBar1.Value = 0

        Dim Sshot As Bitmap = TakeShotOfScreens()
        Dim formats As System.Drawing.Imaging.ImageFormat
        Select Case Mid(ComboBox1.Text, 1, 4)
            Case ".png"
                formats = Drawing.Imaging.ImageFormat.Png
            Case ".jpg"
                formats = Drawing.Imaging.ImageFormat.Jpeg
            Case ".bmp"
                formats = Drawing.Imaging.ImageFormat.Bmp

            Case ".gif"
                formats = Drawing.Imaging.ImageFormat.Gif

            Case Else
                MessageBox.Show("확장자명이 올바르지 않습니다. 올바른 확장자를 넣어 주세요.", "확장자 경고", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboBox1.Focus()
                Exit Sub
        End Select
        last = Date.Today.Month.ToString + "월" + Date.Today.Day.ToString + "일" + Date.Now.Hour.ToString + "시" + Date.Now.Minute.ToString + "분" + Date.Now.Second.ToString + "초" + Mid(ComboBox1.Text, 1, 4)
        CheckedListBox1.Items.Insert(0, last)
        Sshot.Save(Txb_folder.Text + "\" + last, formats)

        Application.DoEvents()



        formats = Nothing
        Sshot = Nothing

        System.GC.Collect()

    End Sub

    Private Sub btn_Opn_Di_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Opn_Di.Click
        If My.Computer.FileSystem.DirectoryExists(Txb_folder.Text) = False Then
            MsgBox("선택하신 경로에 폴더가 없습니다. 만드시겠습니까?", MsgBoxStyle.Critical, "폴더 없음")
            Exit Sub
        End If
        Shell("explorer " + Txb_folder.Text, AppWinStyle.NormalFocus)
    End Sub

    Private Sub CheckedListBox1_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles CheckedListBox1.ItemCheck
        If CheckedListBox1.CheckedItems.Count >= 0 Then
            btn_delet_selected.Enabled = True
        Else
            btn_delet_selected.Enabled = False
        End If
    End Sub



    Private Sub CheckedListBox1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckedListBox1.SelectedIndexChanged
        Try
            If CheckedListBox1.SelectedIndex = -1 Then
                Panel1.Enabled = False
                Pic_image.Image = Nothing
            End If

            If Me.Enabled = False Then
                Pic_image.Image = Nothing
                Exit Sub
            End If
            Pic_image.Image = Image.FromFile(Txb_folder.Text + "\" + CheckedListBox1.SelectedItem.ToString)
            Panel1.Enabled = True
        Catch ex As Exception

        End Try
    End Sub


    Private Sub Pic_image_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Pic_image.Click
        Try
            If Pic_image.Image.Height = 0 Then
                Exit Sub
            End If
            Image_Show.Size = Pic_image.Image.Size
            Image_Show.Text = Mid(CheckedListBox1.SelectedItem.ToString, 1, CheckedListBox1.SelectedItem.ToString.Length - 4) + "에 찍힌 사진"
            Image_Show.PictureBox1.Image = Pic_image.Image
            Image_Show.Show()
            Me.Enabled = False
            Image_Show.BringToFront()
        Catch ex As Exception

        End Try


    End Sub

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ComboBox1.Text = My.Settings.format_Img
        CheckBox1.Checked = My.Settings.Win_start
        CheckBox2.Checked = My.Settings.Draw_time
        NumericUpDown1.Value = My.Settings.num_max
        Txb_folder.Text = My.Settings.last_path
        If Command() = "-hide" Then
            Me.WindowState = FormWindowState.Minimized
            Me.ShowInTaskbar = False
            Me.Hide()
            Me.Visible = False
            Me.Enabled = False
            Me.Hide()
            Button3_Click(Nothing, Nothing)

        End If
        Dim processers() As Process = Process.GetProcessesByName("AutoScreen Shot.exe")
        If processers.Length > 1 Then

            For index = 1 To processers.Length
                If Not processers(index).Id.ToString = Process.GetCurrentProcess.Id.ToString Then
                    processers(index).Kill()
                End If
            Next

        End If
        Try
            If My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run", "AuScShot", Nothing) IsNot Nothing Then
                CheckBox1.Checked = False
            Else
                CheckBox1.Checked = True
            End If
        Catch ex As Exception

        End Try

        Dim i As Integer
        Try
            For i = My.Computer.FileSystem.GetFiles(Txb_folder.Text).Count - 1 To i Step -1
                Select Case Mid(My.Computer.FileSystem.GetFiles(Txb_folder.Text).Item(i), My.Computer.FileSystem.GetFiles(Txb_folder.Text).Item(i).Length - 3, 4)

                    Case ".jpg"
                        CheckedListBox1.Items.Add(My.Computer.FileSystem.GetName(My.Computer.FileSystem.GetFiles(Txb_folder.Text).Item(i)))
                    Case ".png"
                        CheckedListBox1.Items.Add(My.Computer.FileSystem.GetName(My.Computer.FileSystem.GetFiles(Txb_folder.Text).Item(i)))
                    Case ".bmp"
                        CheckedListBox1.Items.Add(My.Computer.FileSystem.GetName(My.Computer.FileSystem.GetFiles(Txb_folder.Text).Item(i)))
                    Case ".gif"
                        CheckedListBox1.Items.Add(My.Computer.FileSystem.GetName(My.Computer.FileSystem.GetFiles(Txb_folder.Text).Item(i)))

                End Select

            Next
        Catch ex As Exception

        End Try

        If Command() = "-hide" Then
            Me.Hide()
            Me.Visible = False
            Me.Enabled = False
            Me.Hide()
            Me.WindowState = FormWindowState.Minimized
            Me.ShowInTaskbar = False
            Button3_Click(Nothing, Nothing)
        End If
        CheckBox1.Checked = My.Settings.Win_start
    End Sub

    Private Sub Btn_Set_all_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Set_all.Click
        For i As Integer = 0 To CheckedListBox1.Items.Count - 1
            CheckedListBox1.SetItemChecked(i, True)
        Next
    End Sub

    Private Sub Btn_Set_non_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Set_non.Click
        For i As Integer = 0 To CheckedListBox1.Items.Count - 1
            CheckedListBox1.SetItemChecked(i, False)
        Next
    End Sub

    Private Sub btn_delet_selected_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_delet_selected.Click
        Pic_image.Image = Nothing
        CheckedListBox1.SelectedItem = Nothing
        System.GC.Collect()
        Dim msgs = MsgBox("체크한 파일을 삭제하려면, '네'를, 현제 선택한 파일만 삭제하려면 '아니오'를 클릭하세요.", MsgBoxStyle.YesNoCancel, "경고")
        Try

A:          If msgs = MsgBoxResult.Yes Then
                Pic_image.Image = Nothing
                Me.Enabled = False
                For i As Integer = CheckedListBox1.Items.Count - 1 To 0 Step -1
                    If CheckedListBox1.GetItemChecked(i) = True Then
                        CheckedListBox1.SelectedItem = Nothing
                        Pic_image.Image = Nothing

                        My.Computer.FileSystem.DeleteFile(Txb_folder.Text + "\" + CheckedListBox1.Items(i))
                        CheckedListBox1.Items.RemoveAt(i)
                        CheckedListBox1.SelectedItem = Nothing
                        Pic_image.Image = Nothing
                        CheckedListBox1.Update()
                    End If
                Next

            End If
            If msgs = MsgBoxResult.No Then
                Pic_image.Image = Nothing
                Me.Enabled = False
                My.Computer.FileSystem.DeleteFile(Txb_folder.Text + "\" + CheckedListBox1.Items(CheckedListBox1.SelectedIndex))
                CheckedListBox1.Items.RemoveAt(CheckedListBox1.SelectedIndex)
            End If

            System.GC.Collect()
        Catch ex As Exception
            CheckedListBox1.SelectedItem = Nothing
            Pic_image.Image = Nothing
            System.GC.Collect()
            If MsgBox("오류가 났습니다. 내용: " + vbCrLf + ex.Message + vbCrLf + " 제시도 하시겠습니까?", MsgBoxStyle.YesNo, "오류") = MsgBoxResult.Yes Then
                GoTo a
            End If
        End Try
        Me.Enabled = True

    End Sub

    Private Sub Btn_refresh_list_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_refresh_list.Click


        Dim i As Integer
        Try
A:
            CheckedListBox1.Items.Clear()


            For i = My.Computer.FileSystem.GetFiles(Txb_folder.Text).Count - 1 To i Step -1
                Select Case Mid(My.Computer.FileSystem.GetFiles(Txb_folder.Text).Item(i), My.Computer.FileSystem.GetFiles(Txb_folder.Text).Item(i).Length - 3, 4)

                    Case ".jpg"
                        CheckedListBox1.Items.Add(My.Computer.FileSystem.GetName(My.Computer.FileSystem.GetFiles(Txb_folder.Text).Item(i)))
                    Case ".png"
                        CheckedListBox1.Items.Add(My.Computer.FileSystem.GetName(My.Computer.FileSystem.GetFiles(Txb_folder.Text).Item(i)))
                    Case ".bmp"
                        CheckedListBox1.Items.Add(My.Computer.FileSystem.GetName(My.Computer.FileSystem.GetFiles(Txb_folder.Text).Item(i)))
                    Case ".gif"
                        CheckedListBox1.Items.Add(My.Computer.FileSystem.GetName(My.Computer.FileSystem.GetFiles(Txb_folder.Text).Item(i)))
                    Case Else

                End Select

            Next

        Catch ex As Exception
            GoTo a
        End Try

        CheckedListBox1.SelectedIndex = -1
        CheckedListBox1_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Private Sub btn_before_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_before.Click
        Try
            If CheckedListBox1.SelectedIndex = 0 Then
                CheckedListBox1.SelectedIndex = CheckedListBox1.Items.Count - 1
                Exit Sub
            End If
            CheckedListBox1.SelectedIndex -= 1
        Catch ex As Exception
            Try
                CheckedListBox1.SelectedIndex = CheckedListBox1.Items.Count - 1
            Catch fex As Exception
                Try
                    CheckedListBox1.SelectedIndex = Nothing
                Catch exs As Exception

                End Try
            End Try
        End Try
    End Sub

    Private Sub btn_next_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_next.Click
        Try
            If CheckedListBox1.SelectedIndex = CheckedListBox1.Items.Count - 1 Then
                CheckedListBox1.SelectedIndex = 0
                Exit Sub
            End If
            CheckedListBox1.SelectedIndex += 1
        Catch ex As Exception
            Try
                CheckedListBox1.SelectedIndex = 0
            Catch fex As Exception
                Try
                    CheckedListBox1.SelectedIndex = Nothing
                Catch exs As Exception

                End Try
            End Try
        End Try
    End Sub

    Private Sub btn_show_defalut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_show_defalut.Click
        Process.Start(Chr(34) + Txb_folder.Text + "\" + CheckedListBox1.SelectedItem.ToString + Chr(34))
    End Sub

    Private Sub btn_delet_picturebox_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_delet_picturebox.Click
        Dim msgs = MsgBox("선택하신 스크린샷 사진을 샥제하시겠습니까?", MsgBoxStyle.YesNo, "삭제 확인")
        If msgs = MsgBoxResult.Yes Then
            Pic_image.Image = Nothing
            Me.Enabled = False
            System.GC.Collect()

            Try
                My.Computer.FileSystem.DeleteFile(Txb_folder.Text + "\" + CheckedListBox1.Items(CheckedListBox1.SelectedIndex))
                CheckedListBox1.Items.RemoveAt(CheckedListBox1.SelectedIndex)
            Catch ex As Exception
                MsgBox("파일 삭제에 실패 하였습니다." + vbCrLf + "오류 내용: " + ex.Message, MsgBoxStyle.Critical, )
            End Try


        End If
        Me.Enabled = True
    End Sub
    Private Sub CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        Try
            If times = 0 Then
                times += 1
                Exit Sub
            End If
            If CheckBox1.Checked = False Then


                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run\", "AuScShot", "")
                Exit Sub
            ElseIf CheckBox1.Checked = True Then
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run\", "AuScShot", Chr(34) + Application.ExecutablePath + Chr(34) + " -hide")
            End If


        Catch ex As Exception

        End Try

        My.Settings.Win_start = CheckBox1.Checked
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Process.Start("http://esukmeansprodev.blogspot.com")
    End Sub

    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim points As New FontDialog
        points.ShowDialog()
        points.Dispose()
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged


        My.Settings.Draw_time = CheckBox2.Checked

    End Sub

    Private Sub ComboBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.TextChanged
        Select Case Mid(ComboBox1.Text, 1, 4)
            Case ".png"
                Exit Select
            Case ".jpg"
                Exit Select
            Case ".bmp"
                Exit Select
            Case ".gif"
                Exit Select
            Case Else
                Exit Sub
        End Select

        My.Settings.format_Img = ComboBox1.Text
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.WindowState = FormWindowState.Minimized
        Me.ShowInTaskbar = False
        Me.Hide()
    End Sub
End Class
