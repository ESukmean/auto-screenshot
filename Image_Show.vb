﻿Public Class Image_Show

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click
        Me.Close()
        Form1.Enabled = True
        Form1.BringToFront()
    End Sub

    Private Sub Image_Show_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Form1.Enabled = True
        Form1.BringToFront()
    End Sub
End Class