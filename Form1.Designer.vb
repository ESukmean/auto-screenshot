﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form은 Dispose를 재정의하여 구성 요소 목록을 정리합니다.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows Form 디자이너에 필요합니다.
    Private components As System.ComponentModel.IContainer

    '참고: 다음 프로시저는 Windows Form 디자이너에 필요합니다.
    '수정하려면 Windows Form 디자이너를 사용하십시오.  
    '코드 편집기를 사용하여 수정하지 마십시오.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.btn_stop_cap = New System.Windows.Forms.Button()
        Me.Btn_start_cap = New System.Windows.Forms.Button()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.btn_Opn_Di = New System.Windows.Forms.Button()
        Me.btn_Sel_Di = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Txb_folder = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TrackBar1 = New System.Windows.Forms.TrackBar()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_delet_picturebox = New System.Windows.Forms.Button()
        Me.btn_show_defalut = New System.Windows.Forms.Button()
        Me.btn_next = New System.Windows.Forms.Button()
        Me.btn_before = New System.Windows.Forms.Button()
        Me.Btn_refresh_list = New System.Windows.Forms.Button()
        Me.btn_delet_selected = New System.Windows.Forms.Button()
        Me.Btn_Set_non = New System.Windows.Forms.Button()
        Me.Btn_Set_all = New System.Windows.Forms.Button()
        Me.CheckedListBox1 = New System.Windows.Forms.CheckedListBox()
        Me.Pic_image = New System.Windows.Forms.PictureBox()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TrackBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.Pic_image, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.CheckBox2)
        Me.GroupBox1.Controls.Add(Me.btn_stop_cap)
        Me.GroupBox1.Controls.Add(Me.Btn_start_cap)
        Me.GroupBox1.Controls.Add(Me.CheckBox1)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.NumericUpDown1)
        Me.GroupBox1.Controls.Add(Me.ComboBox1)
        Me.GroupBox1.Controls.Add(Me.btn_Opn_Di)
        Me.GroupBox1.Controls.Add(Me.btn_Sel_Di)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Txb_folder)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.TrackBar1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(715, 119)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "설정"
        '
        'CheckBox2
        '
        Me.CheckBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Location = New System.Drawing.Point(199, 96)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(168, 16)
        Me.CheckBox2.TabIndex = 6
        Me.CheckBox2.Text = "화면에 부팅 경과시간 표시"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'btn_stop_cap
        '
        Me.btn_stop_cap.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_stop_cap.Enabled = False
        Me.btn_stop_cap.Location = New System.Drawing.Point(557, 92)
        Me.btn_stop_cap.Name = "btn_stop_cap"
        Me.btn_stop_cap.Size = New System.Drawing.Size(75, 23)
        Me.btn_stop_cap.TabIndex = 1
        Me.btn_stop_cap.Text = "캡쳐 중지"
        Me.btn_stop_cap.UseVisualStyleBackColor = True
        '
        'Btn_start_cap
        '
        Me.Btn_start_cap.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Btn_start_cap.Location = New System.Drawing.Point(634, 92)
        Me.Btn_start_cap.Name = "Btn_start_cap"
        Me.Btn_start_cap.Size = New System.Drawing.Size(75, 23)
        Me.Btn_start_cap.TabIndex = 1
        Me.Btn_start_cap.Text = "캡쳐 시작"
        Me.Btn_start_cap.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Checked = True
        Me.CheckBox1.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox1.Location = New System.Drawing.Point(373, 96)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(180, 16)
        Me.CheckBox1.TabIndex = 0
        Me.CheckBox1.Text = "윈도우 시작시 자동으로 실행"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 66)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(73, 12)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "남 은  시 간:"
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.NumericUpDown1.Location = New System.Drawing.Point(433, 38)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {3600, 0, 0, 0})
        Me.NumericUpDown1.Minimum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(120, 21)
        Me.NumericUpDown1.TabIndex = 5
        Me.NumericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDown1.ThousandsSeparator = True
        Me.NumericUpDown1.Value = New Decimal(New Integer() {30, 0, 0, 0})
        '
        'ComboBox1
        '
        Me.ComboBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {".png (화질좋음, 용량 적음)", ".jpg (화질좋음, 용량 많은편)", ".bmp (화질나쁨, 용량 가장 많음)", ".gif (화질나쁨, 용량 가장 적음)"})
        Me.ComboBox1.Location = New System.Drawing.Point(84, 39)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(206, 20)
        Me.ComboBox1.TabIndex = 1
        Me.ComboBox1.Text = ".png (화질좋음, 용량 적음)"
        '
        'btn_Opn_Di
        '
        Me.btn_Opn_Di.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_Opn_Di.Location = New System.Drawing.Point(634, 12)
        Me.btn_Opn_Di.Name = "btn_Opn_Di"
        Me.btn_Opn_Di.Size = New System.Drawing.Size(75, 23)
        Me.btn_Opn_Di.TabIndex = 2
        Me.btn_Opn_Di.Text = "폴더 열기"
        Me.btn_Opn_Di.UseVisualStyleBackColor = True
        '
        'btn_Sel_Di
        '
        Me.btn_Sel_Di.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_Sel_Di.Location = New System.Drawing.Point(556, 12)
        Me.btn_Sel_Di.Name = "btn_Sel_Di"
        Me.btn_Sel_Di.Size = New System.Drawing.Size(75, 23)
        Me.btn_Sel_Di.TabIndex = 2
        Me.btn_Sel_Di.Text = "폴더 선택"
        Me.btn_Sel_Di.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 12)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "파일 확장자:"
        '
        'Txb_folder
        '
        Me.Txb_folder.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Txb_folder.Location = New System.Drawing.Point(84, 13)
        Me.Txb_folder.Name = "Txb_folder"
        Me.Txb_folder.Size = New System.Drawing.Size(467, 21)
        Me.Txb_folder.TabIndex = 1
        Me.Txb_folder.Text = "C:\Scren_Shot"
        '
        'Label1
        '
        Me.Label1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 12)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "저장할 폴더:"
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(296, 43)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(137, 12)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "스크린샷을 저장할 주기:"
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(555, 44)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(140, 12)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "초 (10초~3600초(1시간))"
        '
        'TrackBar1
        '
        Me.TrackBar1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TrackBar1.Enabled = False
        Me.TrackBar1.Location = New System.Drawing.Point(88, 65)
        Me.TrackBar1.Name = "TrackBar1"
        Me.TrackBar1.Size = New System.Drawing.Size(622, 45)
        Me.TrackBar1.TabIndex = 1
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Controls.Add(Me.Panel1)
        Me.GroupBox3.Controls.Add(Me.Btn_refresh_list)
        Me.GroupBox3.Controls.Add(Me.btn_delet_selected)
        Me.GroupBox3.Controls.Add(Me.Btn_Set_non)
        Me.GroupBox3.Controls.Add(Me.Btn_Set_all)
        Me.GroupBox3.Controls.Add(Me.CheckedListBox1)
        Me.GroupBox3.Controls.Add(Me.Pic_image)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 137)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(715, 486)
        Me.GroupBox3.TabIndex = 1
        Me.GroupBox3.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.btn_delet_picturebox)
        Me.Panel1.Controls.Add(Me.btn_show_defalut)
        Me.Panel1.Controls.Add(Me.btn_next)
        Me.Panel1.Controls.Add(Me.btn_before)
        Me.Panel1.Enabled = False
        Me.Panel1.Location = New System.Drawing.Point(194, 402)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(515, 74)
        Me.Panel1.TabIndex = 6
        '
        'btn_delet_picturebox
        '
        Me.btn_delet_picturebox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_delet_picturebox.Location = New System.Drawing.Point(199, 39)
        Me.btn_delet_picturebox.Name = "btn_delet_picturebox"
        Me.btn_delet_picturebox.Size = New System.Drawing.Size(120, 33)
        Me.btn_delet_picturebox.TabIndex = 5
        Me.btn_delet_picturebox.Text = "삭제"
        Me.btn_delet_picturebox.UseVisualStyleBackColor = True
        '
        'btn_show_defalut
        '
        Me.btn_show_defalut.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_show_defalut.Location = New System.Drawing.Point(199, 5)
        Me.btn_show_defalut.Name = "btn_show_defalut"
        Me.btn_show_defalut.Size = New System.Drawing.Size(120, 33)
        Me.btn_show_defalut.TabIndex = 6
        Me.btn_show_defalut.Text = "기본 뷰어로 보기"
        Me.btn_show_defalut.UseVisualStyleBackColor = True
        '
        'btn_next
        '
        Me.btn_next.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_next.Location = New System.Drawing.Point(324, 2)
        Me.btn_next.Name = "btn_next"
        Me.btn_next.Size = New System.Drawing.Size(190, 70)
        Me.btn_next.TabIndex = 3
        Me.btn_next.Text = ">>"
        Me.btn_next.UseVisualStyleBackColor = True
        '
        'btn_before
        '
        Me.btn_before.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_before.Location = New System.Drawing.Point(3, 2)
        Me.btn_before.Name = "btn_before"
        Me.btn_before.Size = New System.Drawing.Size(190, 70)
        Me.btn_before.TabIndex = 4
        Me.btn_before.Text = "<<"
        Me.btn_before.UseVisualStyleBackColor = True
        '
        'Btn_refresh_list
        '
        Me.Btn_refresh_list.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Btn_refresh_list.Location = New System.Drawing.Point(100, 453)
        Me.Btn_refresh_list.Name = "Btn_refresh_list"
        Me.Btn_refresh_list.Size = New System.Drawing.Size(88, 23)
        Me.Btn_refresh_list.TabIndex = 5
        Me.Btn_refresh_list.Text = "새로고침"
        Me.Btn_refresh_list.UseVisualStyleBackColor = True
        '
        'btn_delet_selected
        '
        Me.btn_delet_selected.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_delet_selected.Enabled = False
        Me.btn_delet_selected.Location = New System.Drawing.Point(6, 454)
        Me.btn_delet_selected.Name = "btn_delet_selected"
        Me.btn_delet_selected.Size = New System.Drawing.Size(88, 23)
        Me.btn_delet_selected.TabIndex = 5
        Me.btn_delet_selected.Text = "삭제"
        Me.btn_delet_selected.UseVisualStyleBackColor = True
        '
        'Btn_Set_non
        '
        Me.Btn_Set_non.Location = New System.Drawing.Point(84, 14)
        Me.Btn_Set_non.Name = "Btn_Set_non"
        Me.Btn_Set_non.Size = New System.Drawing.Size(105, 23)
        Me.Btn_Set_non.TabIndex = 4
        Me.Btn_Set_non.Text = "전체 선택 해제"
        Me.Btn_Set_non.UseVisualStyleBackColor = True
        '
        'Btn_Set_all
        '
        Me.Btn_Set_all.Location = New System.Drawing.Point(6, 14)
        Me.Btn_Set_all.Name = "Btn_Set_all"
        Me.Btn_Set_all.Size = New System.Drawing.Size(75, 23)
        Me.Btn_Set_all.TabIndex = 4
        Me.Btn_Set_all.Text = "전체 선택"
        Me.Btn_Set_all.UseVisualStyleBackColor = True
        '
        'CheckedListBox1
        '
        Me.CheckedListBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.CheckedListBox1.FormattingEnabled = True
        Me.CheckedListBox1.Location = New System.Drawing.Point(6, 45)
        Me.CheckedListBox1.Name = "CheckedListBox1"
        Me.CheckedListBox1.Size = New System.Drawing.Size(183, 404)
        Me.CheckedListBox1.TabIndex = 3
        '
        'Pic_image
        '
        Me.Pic_image.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Pic_image.Location = New System.Drawing.Point(195, 14)
        Me.Pic_image.Name = "Pic_image"
        Me.Pic_image.Size = New System.Drawing.Size(514, 382)
        Me.Pic_image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Pic_image.TabIndex = 0
        Me.Pic_image.TabStop = False
        '
        'LinkLabel1
        '
        Me.LinkLabel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(191, 629)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(385, 12)
        Me.LinkLabel1.TabIndex = 2
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "제작자 홈페이지(htp://esukmeansprodev.blogspot.com)로 이동히기"
        '
        'Button2
        '
        Me.Button2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button2.Location = New System.Drawing.Point(582, 624)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(145, 23)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "프로그램 최소화"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(739, 650)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.LinkLabel1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.Text = "AutoScreenShot"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TrackBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.Pic_image, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents btn_Opn_Di As System.Windows.Forms.Button
    Friend WithEvents btn_Sel_Di As System.Windows.Forms.Button
    Friend WithEvents Txb_folder As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Rdb_start As System.Windows.Forms.RadioButton
    Friend WithEvents Rdb_Reg As System.Windows.Forms.RadioButton
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TrackBar1 As System.Windows.Forms.TrackBar
    Friend WithEvents Btn_start_cap As System.Windows.Forms.Button
    Friend WithEvents btn_stop_cap As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Btn_refresh_list As System.Windows.Forms.Button
    Friend WithEvents btn_delet_selected As System.Windows.Forms.Button
    Friend WithEvents Btn_Set_non As System.Windows.Forms.Button
    Friend WithEvents Btn_Set_all As System.Windows.Forms.Button
    Friend WithEvents CheckedListBox1 As System.Windows.Forms.CheckedListBox
    Friend WithEvents Pic_image As System.Windows.Forms.PictureBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btn_delet_picturebox As System.Windows.Forms.Button
    Friend WithEvents btn_show_defalut As System.Windows.Forms.Button
    Friend WithEvents btn_next As System.Windows.Forms.Button
    Friend WithEvents btn_before As System.Windows.Forms.Button
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents Button2 As System.Windows.Forms.Button

End Class
